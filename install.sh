ln -s $PWD/vim/.vimrc  ~/.vimrc
ln -s $PWD/git/.gitconfig ~/.gitconfig

git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
vim +PluginInstall +qall
